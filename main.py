import wmi
from tkinter import *

def free_space():
  c = wmi.WMI()
  list0.delete(0, END)
  for disk in c.Win32_LogicalDisk (DriveType=3):
    disk_list = (disk.Caption + " " + disk.VolumeName)
    list0.insert(END, disk_list)
    print(disk)

def file_system():
  c = wmi.WMI()
  list1.delete(0, END)
  disk = list0.index(ANCHOR)
  choosed = c.Win32_LogicalDisk(DriveType=3)[(disk)]
  print(choosed)
  disk_info = (choosed.Caption, choosed.FileSystem, "%0.2f%% free" % (100.0 * float(choosed.FreeSpace) / float(choosed.Size)))
  list1.insert(END, disk_info)

tk = Tk()

tk.title("Disk Info")

label0 = Label(tk, text="Disk Info")
label0.pack()

button0 = Button(tk, text="Lista dysków", command=free_space)
button0.pack(side="top", fill="x")

button1 = Button(tk, text="Właściwości dysku", command=file_system)
button1.pack(side="top", fill="x")

list0 = Listbox(tk, height=6, width=35)
list0.pack(side="left", fill="x")


list1 = Listbox(tk, height=6, width=35)
list1.pack(side="right", fill="x")

tk.geometry('{}x{}'.format(600, 300))
tk.mainloop()

